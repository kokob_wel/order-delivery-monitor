name := "order-delivery-monitor"

version := "0.0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.apache.commons" % "commons-math3" % "3.0"
libraryDependencies += "com.typesafe" % "config" % "1.3.3"
libraryDependencies += "net.liftweb" %% "lift-json" % "3.4.1"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.1" % Test

parallelExecution in Test := false