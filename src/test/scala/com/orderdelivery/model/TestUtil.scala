package com.orderdelivery.model

/**
  * Created by KOKOB on 3/12/20.
  */
object TestUtil {

  implicit def decayFormula (shelfLife:Int,age:Int,decayRate:Double) : Double = {
    (shelfLife - age) - (decayRate * age)
  }
}
