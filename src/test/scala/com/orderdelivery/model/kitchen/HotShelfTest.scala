package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class HotShelfTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before {
    HotShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("getCurrentCapacity"){
    val initialCapacity = HotShelf.getCurrentCapacity
    HotShelf.addOrderItem(hot)
    assert(HotShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem"){
    val initialCapacity = HotShelf.getCurrentCapacity
    HotShelf.addOrderItem(hot)
    assert(HotShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem: test adding wrong item"){
    assertThrows[RuntimeException]{
      HotShelf.addOrderItem(coldOrder)
    }
    assertThrows[RuntimeException]{
      HotShelf.addOrderItem(frozen)
    }
  }

  test("addOrderItem: test adding null"){
    assertThrows[RuntimeException]{
      HotShelf.addOrderItem(null)
    }
  }

  test("removeOrderItem"){
    HotShelf.addOrderItem(hot)
    val initialCapacity = HotShelf.getCurrentCapacity
    HotShelf.removeOrderItem(hot)
    assert(HotShelf.getCurrentCapacity == initialCapacity+1)
  }

  test("getContentString"){
    HotShelf.addOrderItem(hot)
    assert(HotShelf.getContentString() ==
      "-------------------------------------------HOT--SHELF---------------------------------------------" +
        "\nFoodOrder(name=Pizza, temp=HOT, shelfLife=15, decayRate=0.45, normalizedValue=15.0\n" +
      "-----------------------------------------------------------------------------------------------------")
  }

  test("reshelfItems"){
    val initialCapacity = HotShelf.getCurrentCapacity
    val orderItemWithZeroValue = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)((_,_,_)=>0.0)
    HotShelf.addOrderItem(orderItemWithZeroValue)
    HotShelf.reshelfItems()
    assert(HotShelf.getCurrentCapacity == initialCapacity)
  }
}
