package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class OverflowShelfTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before {
    HotShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("getCurrentCapacity"){
    val initialCapacity = OverflowShelf.getCurrentCapacity
    OverflowShelf.addOrderItem(hot)
    assert(OverflowShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem"){
    val initialCapacity = OverflowShelf.getCurrentCapacity
    OverflowShelf.addOrderItem(hot)
    assert(OverflowShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem: test adding null"){
    assertThrows[RuntimeException]{
      ColdShelf.addOrderItem(null)
    }
  }

  test("removeOrderItem"){
    OverflowShelf.addOrderItem(hot)
    val initialCapacity = OverflowShelf.getCurrentCapacity
    OverflowShelf.removeOrderItem(hot)
    assert(OverflowShelf.getCurrentCapacity == initialCapacity+1)
  }

  test("getContentString"){
    OverflowShelf.addOrderItem(hot)
    assert(OverflowShelf.getContentString() ==
      "-------------------------------------------OVERFLOW--SHELF---------------------------------------------" +
        "\nFoodOrder(name=Pizza, temp=HOT, shelfLife=15, decayRate=0.45, normalizedValue=15.0\n" +
        "-----------------------------------------------------------------------------------------------------")
  }

  test("reshelfItems: testing moving back an order to one of the shelves with storage space."){
    val initialCapacity = OverflowShelf.getCurrentCapacity
    val orderItemWithNonZeroValue = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)((_,_,_)=>1.0)
    OverflowShelf.addOrderItem(orderItemWithNonZeroValue)
    OverflowShelf.reshelfItems()
    assert(OverflowShelf.getCurrentCapacity == initialCapacity)
    assert(HotShelf.removeOrderItem(orderItemWithNonZeroValue).get==orderItemWithNonZeroValue)
  }

}
