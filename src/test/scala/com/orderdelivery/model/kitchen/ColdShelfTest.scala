package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class ColdShelfTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before {
    ColdShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("getCurrentCapacity"){
    val initialCapacity = ColdShelf.getCurrentCapacity
    ColdShelf.addOrderItem(coldOrder)
    assert(ColdShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem"){
    val initialCapacity = ColdShelf.getCurrentCapacity
    ColdShelf.addOrderItem(coldOrder)
    assert(ColdShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem: test adding wrong item"){
    assertThrows[RuntimeException]{
      ColdShelf.addOrderItem(hot)
    }
    assertThrows[RuntimeException]{
      ColdShelf.addOrderItem(frozen)
    }
  }

  test("addOrderItem: test adding null"){
    assertThrows[RuntimeException]{
      ColdShelf.addOrderItem(null)
    }
  }

  test("removeOrderItem"){
    ColdShelf.addOrderItem(coldOrder)
    val initialCapacity = ColdShelf.getCurrentCapacity
    ColdShelf.removeOrderItem(coldOrder)
    assert(ColdShelf.getCurrentCapacity == initialCapacity+1)
  }

  test("getContentString"){
    ColdShelf.addOrderItem(coldOrder)
    assert(ColdShelf.getContentString() ==
      "-------------------------------------------COLD--SHELF---------------------------------------------" +
        "\nFoodOrder(name=Chicken Salad, temp=COLD, shelfLife=35, decayRate=0.35, normalizedValue=35.0\n" +
      "-----------------------------------------------------------------------------------------------------")
  }

  test("reshelfItems"){
    val initialCapacity = ColdShelf.getCurrentCapacity
    val orderItemWithZeroValue = new FoodOrder("Pizza",TEMPERATURE.COLD,15,0.45)((_,_,_)=>0.0)
    ColdShelf.addOrderItem(orderItemWithZeroValue)
    ColdShelf.reshelfItems()
    assert(ColdShelf.getCurrentCapacity == initialCapacity)
  }

  test("clearShelf"){
    ColdShelf.addOrderItem(coldOrder)
    val initialCapacity = ColdShelf.getCurrentCapacity
    ColdShelf.clearShelf()
    assert(ColdShelf.getCurrentCapacity == initialCapacity+1)
  }

  test("clearShelf: Test sending order to overflow shelf when there is no storage available"){
    ColdShelf.addOrderItem(coldOrder)
    val initialCapacity = ColdShelf.getCurrentCapacity
    ColdShelf.clearShelf()
    assert(ColdShelf.getCurrentCapacity == initialCapacity+1)
  }
}
