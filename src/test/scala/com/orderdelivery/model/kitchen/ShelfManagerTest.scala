package com.orderdelivery.model.kitchen

import com.orderdelivery.config.AppConfig
import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class ShelfManagerTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before {
    ColdShelf.clearShelf()
    HotShelf.clearShelf()
    FrozenShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("putOrder"){
    ShelfManager.putOrder(coldOrder)
    assert(ColdShelf.getCurrentCapacity == AppConfig.coldShelfCapacity-1)
    ShelfManager.putOrder(hot)
    assert(HotShelf.getCurrentCapacity == AppConfig.hotShelfCapacity-1)
    ShelfManager.putOrder(frozen)
    assert(FrozenShelf.getCurrentCapacity == AppConfig.frozenShelfCapacity-1)
  }

  test("putOrder: test put null"){
    assertThrows[RuntimeException]{
      ShelfManager.putOrder(null)
    }
  }


  test("pickUpOrder"){
    ShelfManager.putOrder(coldOrder)
    ShelfManager.pickUpOrder(coldOrder)
    assert(ColdShelf.getCurrentCapacity == AppConfig.coldShelfCapacity)
    ShelfManager.putOrder(hot)
    ShelfManager.pickUpOrder(hot)
    assert(HotShelf.getCurrentCapacity == AppConfig.hotShelfCapacity)
    ShelfManager.putOrder(frozen)
    ShelfManager.pickUpOrder(frozen)
    assert(FrozenShelf.getCurrentCapacity == AppConfig.frozenShelfCapacity)
  }

  test("pickUpOrder: test put null"){
    assertThrows[RuntimeException]{
      ShelfManager.pickUpOrder(null)
    }
  }


  test("reShelfAll"){
    ColdShelf.addOrderItem(coldOrder)
    HotShelf.addOrderItem(hot)
    FrozenShelf.addOrderItem(frozen)
    OverflowShelf.addOrderItem(hot)
    ShelfManager.reShelfAll()
    assert(ColdShelf.getCurrentCapacity == AppConfig.coldShelfCapacity-1)
    assert(HotShelf.getCurrentCapacity == AppConfig.hotShelfCapacity-2)
    assert(FrozenShelf.getCurrentCapacity == AppConfig.frozenShelfCapacity-1)
    assert(OverflowShelf.getCurrentCapacity == AppConfig.overflowShelfCapacity)
  }

  test("clearAllShelves"){
    ColdShelf.addOrderItem(coldOrder)
    HotShelf.addOrderItem(hot)
    FrozenShelf.addOrderItem(frozen)
    OverflowShelf.addOrderItem(hot)
    ShelfManager.clearAllShelves()
    assert(ColdShelf.getCurrentCapacity == AppConfig.coldShelfCapacity)
    assert(HotShelf.getCurrentCapacity == AppConfig.hotShelfCapacity)
    assert(FrozenShelf.getCurrentCapacity == AppConfig.frozenShelfCapacity)
    assert(OverflowShelf.getCurrentCapacity == AppConfig.overflowShelfCapacity)
  }
}