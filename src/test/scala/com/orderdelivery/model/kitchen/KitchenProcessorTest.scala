package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class KitchenProcessorTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before{
    ColdShelf.clearShelf()
    HotShelf.clearShelf()
    FrozenShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("Test sendOrderToShelf"){
    val initialShelfCapacity = ColdShelf.getCurrentCapacity
    val kitchenProcessor = new KitchenProcessor()
    kitchenProcessor.publish(coldOrder)
    assert(ColdShelf.getCurrentCapacity == initialShelfCapacity-1)
  }

  test("test publish null order"){
    val kitchenProcessor = new KitchenProcessor()
    assertThrows[RuntimeException]{
      kitchenProcessor.publish(null)
    }
  }
}
