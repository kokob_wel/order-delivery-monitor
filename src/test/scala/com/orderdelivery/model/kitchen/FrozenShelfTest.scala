package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.model.order.FoodOrder
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class FrozenShelfTest extends AnyFunSuite  with BeforeAndAfter{

  val coldOrder: OrderItem = new FoodOrder("Chicken Salad",TEMPERATURE.COLD,35,0.35)(TestUtil.decayFormula)
  val hot: OrderItem = new FoodOrder("Pizza",TEMPERATURE.HOT,15,0.45)(TestUtil.decayFormula)
  val frozen: OrderItem = new FoodOrder("Ice Cream",TEMPERATURE.FROZEN,60,0.23)(TestUtil.decayFormula)

  before {
    FrozenShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  test("getCurrentCapacity"){
    val initialCapacity = FrozenShelf.getCurrentCapacity
    FrozenShelf.addOrderItem(frozen)
    assert(FrozenShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem"){
    val initialCapacity = FrozenShelf.getCurrentCapacity
    FrozenShelf.addOrderItem(frozen)
    assert(FrozenShelf.getCurrentCapacity == initialCapacity-1)
  }

  test("addOrderItem: test adding wrong item"){
    assertThrows[RuntimeException]{
      FrozenShelf.addOrderItem(hot)
    }
    assertThrows[RuntimeException]{
      FrozenShelf.addOrderItem(coldOrder)
    }
  }

  test("addOrderItem: test adding null"){
    assertThrows[RuntimeException]{
      FrozenShelf.addOrderItem(null)
    }
  }

  test("removeOrderItem"){
    FrozenShelf.addOrderItem(frozen)
    val initialCapacity = FrozenShelf.getCurrentCapacity
    FrozenShelf.removeOrderItem(frozen)
    assert(FrozenShelf.getCurrentCapacity == initialCapacity+1)
  }

  test("getContentString"){
    FrozenShelf.addOrderItem(frozen)
    assert(FrozenShelf.getContentString() ==
      "-------------------------------------------FROZEN--SHELF---------------------------------------------" +
        "\nFoodOrder(name=Ice Cream, temp=FROZEN, shelfLife=60, decayRate=0.23, normalizedValue=60.0\n" +
      "-----------------------------------------------------------------------------------------------------")
  }

  test("reshelfItems"){
    val initialCapacity = FrozenShelf.getCurrentCapacity
    val orderItemWithZeroValue = new FoodOrder("Pizza",TEMPERATURE.FROZEN,15,0.45)((_,_,_)=>0.0)
    FrozenShelf.addOrderItem(orderItemWithZeroValue)
    FrozenShelf.reshelfItems()
    assert(FrozenShelf.getCurrentCapacity == initialCapacity)
  }

}
