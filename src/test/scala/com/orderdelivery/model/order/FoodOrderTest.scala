package com.orderdelivery.model.order

import com.orderdelivery.model.TestUtil
import com.orderdelivery.model.enums.TEMPERATURE
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class FoodOrderTest extends AnyFunSuite  with BeforeAndAfter{

  test("Food order constructor"){
      val sampleOrderName = "TestOrderName"
      val testTemperature = TEMPERATURE.HOT
      val sampleShelfLife = 45
      val sampleDecayRate = 0.38
      val testWaitTimeInSeconds = 2
      val expectedItemValueAfterWaitTime = 42.24
      val foodOrder = new FoodOrder(sampleOrderName,testTemperature,sampleShelfLife,sampleDecayRate)(TestUtil.decayFormula)
      Thread.sleep(1000*testWaitTimeInSeconds)
      assert(expectedItemValueAfterWaitTime == foodOrder.getValue)
      assert(sampleOrderName == foodOrder.getName())
      assert(testTemperature == foodOrder.getTemp())
      assert(sampleShelfLife == foodOrder.getShelfLife())
      assert(sampleDecayRate == foodOrder.getDecayRate())
  }

  test("getAgeInSeconds"){
      val sampleOrderName = "TestOrderName"
      val testTemperature = TEMPERATURE.COLD
      val sampleShelfLife = 45
      val sampleDecayRate = 0.38
      val testWaitTimeInSeconds = 2
      val expectedItemAgeInSeconds = 2
      val foodOrder = new FoodOrder(sampleOrderName,testTemperature,sampleShelfLife,sampleDecayRate)(TestUtil.decayFormula)
      Thread.sleep(1000*testWaitTimeInSeconds)
      assert(expectedItemAgeInSeconds == foodOrder.getAgeInSeconds())
  }

}