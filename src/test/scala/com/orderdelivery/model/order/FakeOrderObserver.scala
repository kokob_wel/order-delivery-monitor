package com.orderdelivery.model.order

import com.orderdelivery.interfaces.{OrderItem, OrderObserver}
import scala.collection.mutable.ArrayBuffer

/**
  * Created by KOKOB on 3/11/20.
  */
object FakeOrderObserver extends OrderObserver {

  val observedOrderItems: ArrayBuffer[OrderItem] = ArrayBuffer[OrderItem]()

  override def publish(orderItem: OrderItem): Unit = {
    observedOrderItems += orderItem
  }
}
