package com.orderdelivery.model.order

import com.orderdelivery.config.AppConfig
import com.orderdelivery.model.kitchen.{ColdShelf, FrozenShelf, HotShelf, OverflowShelf}
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class JsonFileOrderProducerTest extends AnyFunSuite with BeforeAndAfter{

  before {
    ColdShelf.clearShelf()
    HotShelf.clearShelf()
    FrozenShelf.clearShelf()
    OverflowShelf.clearShelf()
    FakeOrderObserver.observedOrderItems.clear()
  }

  test("processOrders"){
    val orderProcessor: JsonFileOrderProducer = new JsonFileOrderProducer()
    orderProcessor.registerObserver(FakeOrderObserver)
    orderProcessor.processOrders("src/test/resources/Test_Orders.json")
    val numOfOrderItemsInTestOrders = 3
    Thread.sleep(1000*numOfOrderItemsInTestOrders)
    assert(numOfOrderItemsInTestOrders == FakeOrderObserver.observedOrderItems.size)
  }
}

