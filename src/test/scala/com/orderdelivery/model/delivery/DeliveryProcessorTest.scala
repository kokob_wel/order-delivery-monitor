package com.orderdelivery.model.delivery

import java.util.concurrent.Executors
import org.scalatest.BeforeAndAfter
import org.scalatest.funsuite.AnyFunSuite

/**
  * Created by KOKOB on 3/11/20.
  */
class DeliveryProcessorTest extends AnyFunSuite  with BeforeAndAfter{

    test("test publish null order"){
      val deliveryExecutorService = Executors.newSingleThreadExecutor()
      val deliveryProcessor = new DeliveryProcessor(deliveryExecutorService)
      assertThrows[RuntimeException]{
        deliveryProcessor.publish(null)
      }
    }
}