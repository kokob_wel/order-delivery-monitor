package com.orderdelivery.app

import java.util.concurrent.Executors
import com.orderdelivery.config.AppConfig
import com.orderdelivery.model.delivery.DeliveryProcessor
import com.orderdelivery.model.kitchen.KitchenProcessor
import com.orderdelivery.model.order.JsonFileOrderProducer

/**
  * Created by KOKOB WELDETENSAE on 3/7/20.
  * Main entry class to start the application.
  */
object MainApp {

  /**
    * Executor service for delivery processing.
    */
  val deliveryExecutorService = Executors.newCachedThreadPool()

  /**
    * Main entry method.This method instantiates both the observer instances and observable instance, registers all the -
    * observers to the appropriate observable (order processing) instance, launches order processing and terminates the -
    * process by shutting down all delivery executions when every they complete their task.
    *
    * @param args input arguments. No input argument is required.
    */
  def main(args: Array[String]): Unit = {
    val kitchenProcessor: KitchenProcessor = new KitchenProcessor()
    val deliveryProcessor: DeliveryProcessor = new DeliveryProcessor(deliveryExecutorService)
    val orderProcessor: JsonFileOrderProducer = new JsonFileOrderProducer()
    orderProcessor.registerObserver(kitchenProcessor)
    orderProcessor.registerObserver(deliveryProcessor)
    orderProcessor.processOrders(AppConfig.inputFilePath)
    deliveryExecutorService.shutdown()
  }
}