package com.orderdelivery.interfaces

/**
  * Created by KOKOB WELDETENSAE on 3/7/20.
  * Producer interface.
  */
trait ObservableOrderProducer {

  /**
   * Method to register all observers of any instance that extends this trait.
   * @param observer Observer of this class.
   */
  def registerObserver(observer: OrderObserver)

  /**
   * A method to notify all registered observers or consumers.
   * @param orderItem order item that needs to be sent to observers.
   */
  def notifyObservers(orderItem: OrderItem)
}
