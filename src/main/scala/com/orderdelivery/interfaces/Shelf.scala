package com.orderdelivery.interfaces

/**
  * Created by KOKOB WELDETENSAE on 3/6/20.
  * Shelf interface.d
  */
trait Shelf {

  /**
   * Function that computes current capacity of the shelf.
   * @return Current available space in the shelf.
   */
  def getCurrentCapacity: Int

  /**
   * Method that adds order item in to a shelf.
   * @param orderItem item to be placed in this shelf.
   */
  def addOrderItem(orderItem: OrderItem): Unit

  /**
   * Method to remove a specific order item from the shelf.
   * @param orderItem order item that needs to be removed.
   */
  def removeOrderItem(orderItem: OrderItem): Option[OrderItem]

  /**
   * Method that returns the state of the shelf in String format.
   * @return String representation of the shelf.
   */
  def getContentString(): String

  /**
   * Method that handles the re-arrangement of order items.
   */
  def reshelfItems()

  /**
   * Method that deletes all orders in the shelf.
   */
  def clearShelf()
}
