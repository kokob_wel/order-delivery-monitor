package com.orderdelivery.interfaces

/**
  * Created by KOKOB WELDETENSAE on 3/9/20.
  * Consumer interface.
  */
trait OrderObserver {

  /**
   * Method that observes or subscribes to any update from producer or observable entity.
   * @param orderItem order item that has been placed.
   */
  def publish(orderItem: OrderItem): Unit
}
