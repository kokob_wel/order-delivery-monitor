package com.orderdelivery.interfaces

import com.orderdelivery.model.enums.TEMPERATURE.TEMP

/**
  * Created by KOKOB WELDETENSAE on 3/6/20.
  * Order item interface.
  */
trait OrderItem {

  /**
   * Function that computes current value of this order.
   * @return current value of this order.
   */
  def getValue() : Double

  /**
   * Method that calculates a real time age of this order.
   * @return current age of this order.
   */
  def getAgeInSeconds() : Int

  /**
   * Temperature accessor.
   * @return Temperature
   */
  def getTemp(): TEMP

  /**
   * Order name accessor.
   * @return order name.
   */
  def getName(): String

  /**
   * ShelfLife accessor.
   * @return shelfLife.
   */
  def getShelfLife(): Int

  /**
   * DecayRate accessor.
   * @return decayRate.
   */
  def getDecayRate(): Double

  /**
   * String format of order item.
   * @return String representation of order item.
   */
  def toString(): String
}