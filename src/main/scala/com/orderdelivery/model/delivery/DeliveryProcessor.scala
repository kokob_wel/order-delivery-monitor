package com.orderdelivery.model.delivery

import java.util.concurrent.ExecutorService
import com.orderdelivery.config.AppConfig
import com.orderdelivery.model.kitchen.ShelfManager
import com.orderdelivery.interfaces.{OrderItem, OrderObserver}
import scala.util.Random

/**
  * Created by KOKOB WELDETENSAE on 3/9/20.
  * Delivery processing class. This class is an observer of Order Processor.
  */
class DeliveryProcessor(executorService : ExecutorService) extends OrderObserver {

  /**
    * Private method that performs picking up of an order.
    * @param orderItem item that needs to be picked up.
    * @return Optional item that has been picked up.
    */
  def pickOrderFromShelf(orderItem: OrderItem) = ShelfManager.pickUpOrder(orderItem)

  /**
    * Method that observes or subscribes to any update from order processing observable instance.
    * @param orderItem order item that has been placed.
    */
  override def publish(orderItem: OrderItem): Unit = {
    if(orderItem==null)
      throw new RuntimeException("Invalid order in delivery processor:" + orderItem)
    executorService.execute(()=> {
      val trafficDelaySeconds = Random.nextInt(AppConfig.maxTrafficDelayInSeconds)+AppConfig.minTrafficDelayInSeconds
      Thread.sleep(trafficDelaySeconds*1000)
      ShelfManager.reShelfAll()
      pickOrderFromShelf(orderItem)
      ShelfManager.displayAllShelves()
    })
  }

}