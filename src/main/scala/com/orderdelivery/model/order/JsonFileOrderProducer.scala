package com.orderdelivery.model.order

import com.orderdelivery.config.AppConfig
import com.orderdelivery.model.enums.TEMPERATURE
import com.orderdelivery.interfaces.{ObservableOrderProducer, OrderItem, OrderObserver}
import net.liftweb.json._
import org.apache.commons.math3.distribution.PoissonDistribution
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Created by KOKOB WELDETENSAE on 3/7/20.
  */
class JsonFileOrderProducer() extends ObservableOrderProducer {

  /**
    * Predefined rate of order flow.
    */
  private val poissonDistributionRate = AppConfig.poissonDistributionRate

  /**
    * A pre-defined function which computes a value of an item at any given time.
    * @param shelfLife shelfLife of an item.
    * @param age age of an item from the time of creation.
    * @param decayRate decay rate of an item.
    * @return value of the item.
    */
  implicit def decayFormula (shelfLife:Int,age:Int,decayRate:Double) : Double = {
    (shelfLife - age) - (decayRate * age)
  }

  val observerList: ArrayBuffer[OrderObserver] = ArrayBuffer[OrderObserver]()

  /**
    * Method to register all observers of this class.
    * @param observer Observer of this class.
    */
  def registerObserver(observer: OrderObserver) = {
    observerList += observer
  }

  /**
    * Method to un-register or remove a specific observer.
    * @param observer Observer to be un-registered.
    */
  def unregister(observer: OrderObserver) = {
    val index: Int = observerList.indexOf(observer)
    observerList.remove(index)
  }

  /**
    * A method to notify all registered observers.
    * @param orderItem order item that needs to be sent to observers.
    */
  override def notifyObservers(orderItem: OrderItem) = {
    for (observer <- observerList) {
      observer.publish(orderItem)
    }
  }

  /**
    * An entry method that initiates the processing of an order. This method launches an Asynchronous process to generate
    * orders at a configured Poisson Distribution rate and invokes notify all method to notify all observers.
    */
  def processOrders(inputFilePath: String) = {
    val orderProcessingThread = new Thread(()=> {
      val poissonDistribution: PoissonDistribution = new PoissonDistribution(poissonDistributionRate)
      val jsonSource = Source.fromFile(inputFilePath)
      val json = parse(jsonSource.getLines().mkString)
      val elements = json.children
      implicit val formats = net.liftweb.json.DefaultFormats
      for (e <- elements) {
        val poissonSample = poissonDistribution.sample()
        val poissonRate = if (poissonSample==0) 1 else poissonSample
        Thread.sleep(1000/poissonRate)
        val order:JsonOrderItem = e.extract[JsonOrderItem]
        notifyObservers(new FoodOrder(order.name,TEMPERATURE.getTempEnum(order.temp),order.shelfLife,order.decayRate))
      }
    })
    orderProcessingThread.start()
    orderProcessingThread.join()
  }

  /**
    * Entity class designed to parse json schema that will be used by liftweb.json library.
    * @param name order name
    * @param temp order temperature
    * @param shelfLife shelf life
    * @param decayRate decay rate
    */
  case class JsonOrderItem(name:String,temp:String,shelfLife:Int,decayRate:Double)
}