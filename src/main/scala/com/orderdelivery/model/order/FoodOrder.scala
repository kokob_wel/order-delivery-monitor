package com.orderdelivery.model.order

import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.enums.TEMPERATURE.TEMP

/**
  * Created by KOKOB WELDETENSAE on 3/7/20.
  */
class FoodOrder(name:String,
                     temp:TEMP,
                     shelfLife:Int,
                     decayRate:Double)(implicit decayFormula: (Int,Int,Double) => Double) extends OrderItem {

  private val madeTime:Long = System.currentTimeMillis()

  /**
    * Function that invokes a decay formula to compute current value of this order.
    * @return current value of this order.
    */
  def getValue = decayFormula(shelfLife,getAgeInSeconds(),decayRate)

  /**
    * Method that calculates a real time age of this order.
    * @return current age of this order.
    */
  override def getAgeInSeconds(): Int = {
    val currentAge = (System.currentTimeMillis() - madeTime)
    currentAge.toInt/1000
  }

  /**
    * Temperature accessor.
    * @return Temperature
    */
  override def getTemp(): TEMP = temp

  /**
    * Order name accessor.
    * @return order name.
    */
  override def getName(): String = name

  /**
    * shelfLife accessor.
    * @return shelfLife.
    */
  override def getShelfLife(): Int = shelfLife

  /**
    * decayRate accessor.
    * @return decayRate.
    */
  override def getDecayRate(): Double = decayRate

  /**
    * String format of order item.
    * @return String representation of order item.
    */
  override def toString = s"FoodOrder(name=$name, temp=$temp, shelfLife=$shelfLife, decayRate=$decayRate, normalizedValue=$getValue"

}