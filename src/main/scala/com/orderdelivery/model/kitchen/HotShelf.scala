package com.orderdelivery.model.kitchen

import com.orderdelivery.config.AppConfig
import com.orderdelivery.interfaces.{OrderItem, Shelf}
import com.orderdelivery.model.enums.TEMPERATURE
import scala.collection.mutable.Queue

/**
  * Created by KOKOB WELDETENSAE on 3/6/20.
  */
object HotShelf extends Shelf{

  private val capacity: Int = AppConfig.hotShelfCapacity
  private val shelfQueue = new Queue[OrderItem]()
  private val shelfTemperature = TEMPERATURE.HOT

  /**
    * Function that computes current capacity of the shelf.
    * @return Current available space in this shelf.
    */
  override def getCurrentCapacity: Int = capacity - shelfQueue.size

  /**
    * Method that adds order item in to this shelf. Checks for available space, if full sends item to overflow shelf.
    * @param orderItem item to be placed in this shelf.
    */
  override def addOrderItem(orderItem: OrderItem) = {
    if(orderItem==null || orderItem.getTemp()!=this.shelfTemperature)
      throw new RuntimeException("Invalid order in hot shelf:" + orderItem)
    if(getCurrentCapacity > 0)
      shelfQueue.enqueue(orderItem)
    else if(getCurrentCapacity <= 0 && OverflowShelf.getCurrentCapacity > 0)
      OverflowShelf.addOrderItem(orderItem)
    else {
      //Do Noting: Order Item is now considered waste.
    }
  }

  /**
    * Method to remove a specific order item from the shelf.
    * @param orderItem order item that needs to be removed.
    */
  override def removeOrderItem(orderItem: OrderItem): Option[OrderItem] = {
    shelfQueue.dequeueFirst(item=>item.equals(orderItem))
  }

  /**
    * Method that returns the state of the shelf in String format.
    * @return String representation of the shelf.
    */
  override def getContentString(): String = {
    var contentString = ""
    for (elem <- shelfQueue) {
      contentString += elem.toString() + "\n"
    }
    ShelfManager.formatShelfDisplay(contentString,TEMPERATURE.HOT.toString)
  }

  /**
    * Method that handles the removal of order items whose value has degraded below zero.
    */
  override def reshelfItems() = {
    shelfQueue.dequeueAll(orderItem => orderItem.getValue()<=0)
  }

  /**
    * Method that deletes all orders in the shelf.
    */
  override def clearShelf() = {
    shelfQueue.clear()
  }
}