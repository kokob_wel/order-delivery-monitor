package com.orderdelivery.model.kitchen

import com.orderdelivery.config.AppConfig
import com.orderdelivery.interfaces.{OrderItem, Shelf}
import com.orderdelivery.model.enums.TEMPERATURE
import scala.collection.mutable.Queue

/**
  * Created by KOKOB WELDETENSAE on 3/6/20.
  */
object OverflowShelf extends Shelf {

  private val capacity: Int = AppConfig.overflowShelfCapacity
  private val shelfQueue = new Queue[OrderItem]()
  private val supportedOrderTemperatures = TEMPERATURE.values

  /**
    * Function that computes current capacity of the shelf.
    * @return Current available space in this shelf.
    */
  override def getCurrentCapacity: Int = capacity - shelfQueue.size

  /**
    * Method that adds order item in to this shelf. Checks for available space, if full sends item to overflow shelf.
    * @param orderItem item to be placed in this shelf.
    */
  override def addOrderItem(orderItem: OrderItem) =  {
    if(orderItem==null || !supportedOrderTemperatures.contains(orderItem.getTemp()))
      throw new RuntimeException("Invalid order in overflow shelf:" + orderItem)
    if(getCurrentCapacity > 0){
      shelfQueue.enqueue(orderItem)
    }
  }

  /**
    * Method to remove a specific order item from the shelf.
    * @param orderItem order item that needs to be removed.
    */
  override def removeOrderItem(orderItem: OrderItem): Option[OrderItem] = {
    shelfQueue.dequeueFirst(item=>item.equals(orderItem))
  }

  /**
    * Method that returns the state of the shelf in String format.
    * @return String representation of the shelf.
    */
  override def getContentString(): String = {
    var contentString = ""
    for (elem <- shelfQueue) {
      contentString += elem.toString() + "\n"
    }
    ShelfManager.formatShelfDisplay(contentString,"OVERFLOW")
  }

  /**
    * Method that reshelf the food items. First it removes all order items whose values are less than or equal to 0.
    * Then it transports order items from the overflow shelf to an appropriate shelf.
    */
  override def reshelfItems() =  {

    shelfQueue.dequeueAll(orderItem => orderItem.getValue()<=0)

    if(ColdShelf.getCurrentCapacity>0 && shelfQueue.exists(orderItem => orderItem.getTemp().equals(TEMPERATURE.COLD))){
      val orderItem  = shelfQueue.dequeueFirst(orderItem => orderItem.getTemp().equals(TEMPERATURE.COLD)).get
      ColdShelf.addOrderItem(orderItem)
    }
    if(HotShelf.getCurrentCapacity>0 && shelfQueue.exists(orderItem => orderItem.getTemp().equals(TEMPERATURE.HOT))){
      val orderItem  = shelfQueue.dequeueFirst(orderItem => orderItem.getTemp().equals(TEMPERATURE.HOT)).get
      HotShelf.addOrderItem(orderItem)
    }
    if(FrozenShelf.getCurrentCapacity>0 && shelfQueue.exists(orderItem => orderItem.getTemp().equals(TEMPERATURE.FROZEN))){
      val orderItem  = shelfQueue.dequeueFirst(orderItem => orderItem.getTemp().equals(TEMPERATURE.FROZEN)).get
      FrozenShelf.addOrderItem(orderItem)
    }
  }

  /**
    * Method that deletes all orders in the shelf.
    */
  override def clearShelf() = {
    shelfQueue.clear()
  }
}