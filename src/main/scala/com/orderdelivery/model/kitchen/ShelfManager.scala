package com.orderdelivery.model.kitchen

import com.orderdelivery.config.AppConfig
import com.orderdelivery.interfaces.OrderItem
import com.orderdelivery.model.enums.TEMPERATURE

/**
  * Created by KOKOB WELDETENSAE on 3/9/20.
  * Helper singleton object that bridges requests from the actual shelves.
  */
object ShelfManager {

  /**
    * Method that facilitates putting food items in to one of the shelves.
    * @param orderItem order item instance.
    */
  def putOrder(orderItem: OrderItem)  =  this.synchronized {
    if(orderItem==null)
      throw new RuntimeException("Invalid order detected in shelf manager instance putOrder function:" + orderItem)
    orderItem.getTemp() match {
      case TEMPERATURE.COLD => ColdShelf.addOrderItem(orderItem)
      case TEMPERATURE.HOT => HotShelf.addOrderItem(orderItem)
      case TEMPERATURE.FROZEN => FrozenShelf.addOrderItem(orderItem)
    }

  }

  /**
    * Method that facilitates the removal of food items from one of the shelves.
    * @param orderItem order item instance.
    */
  def pickUpOrder(orderItem: OrderItem) : Option[OrderItem] = this.synchronized {
    if(orderItem==null)
      throw new RuntimeException("Invalid order detected in shelf manager instance pickUpOrder function:" + orderItem)
    val pickedUpOrder = orderItem.getTemp() match {
      case TEMPERATURE.COLD => ColdShelf.removeOrderItem(orderItem)
      case TEMPERATURE.HOT => HotShelf.removeOrderItem(orderItem)
      case TEMPERATURE.FROZEN => FrozenShelf.removeOrderItem(orderItem)
    }
    if(!pickedUpOrder.isDefined && AppConfig.allowPickUpFromOverflow)
        return OverflowShelf.removeOrderItem(orderItem)
    return pickedUpOrder
  }

  /**
    * Method that performs display data operation.
    */
  def displayAllShelves() = this.synchronized {
    println(ColdShelf.getContentString() + "\n" +
          HotShelf.getContentString() + "\n" +
          FrozenShelf.getContentString() +"\n" +
          OverflowShelf.getContentString()
    )
  }

  /**
    * Method that handle the re-shelving process. This includes, removing food items with value less than or equal to 0.
    * Moving back food items from overflow shelf to its respective shelf.
    */
  def reShelfAll() =  this.synchronized {
    ColdShelf.reshelfItems()
    FrozenShelf.reshelfItems()
    HotShelf.reshelfItems()
    OverflowShelf.reshelfItems()
  }

  /**
    * Method that clears all the shelves.
    */
  def clearAllShelves() =  this.synchronized {
    ColdShelf.clearShelf()
    FrozenShelf.clearShelf()
    HotShelf.clearShelf()
    OverflowShelf.clearShelf()
  }

  /**
    * Shelf Display utility method that modularize string formatting.
    * @param shelfStringFormat String format of all available items in a shelf.
    * @param category type of shelf.
    * @return
    */
  def formatShelfDisplay(shelfStringFormat: String, category: String) : String = this.synchronized {
    s"-------------------------------------------$category--SHELF---------------------------------------------" +
     "\n" + shelfStringFormat +
    "-----------------------------------------------------------------------------------------------------"
  }
}