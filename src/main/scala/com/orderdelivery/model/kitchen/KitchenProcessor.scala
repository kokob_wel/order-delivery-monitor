package com.orderdelivery.model.kitchen

import com.orderdelivery.interfaces.{OrderItem, OrderObserver}

/**
  * Created by KOKOB WELDETENSAE on 3/9/20.
  */
class KitchenProcessor extends OrderObserver {

  /**
    * Private method that makes the food instantly and sends it to a shelf.
    * @param orderItem item that needs to be placed.
    */
  private def sendOrderToShelf(orderItem: OrderItem) = ShelfManager.putOrder(orderItem)

  /**
    * Method that observes or subscribes to any update from order processing observable instance.
    * @param orderItem order item that has been placed.
    */
  override def publish(orderItem: OrderItem): Unit = {
    if(orderItem==null)
      throw new RuntimeException("Invalid order in kitchen processor:" + orderItem)
    val kitchenThread = new Thread(()=> {
      sendOrderToShelf(orderItem)
      ShelfManager.reShelfAll()
      ShelfManager.displayAllShelves()
    })
    kitchenThread.start()
    kitchenThread.join()
  }
}