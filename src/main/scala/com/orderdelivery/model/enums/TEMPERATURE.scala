package com.orderdelivery.model.enums

/**
  * Created by KOKOB WELDETENSAE on 3/7/20.
  */
object TEMPERATURE extends Enumeration {
    type TEMP = Value
    val HOT,COLD,FROZEN = Value

    /**
      * Utility method to convert string literal to an equivalent Temperature Enum.
      * @param name String literal of supported food temperatures, case insensitive.
      * @return appropriate Temperature Enum. Throws RunTime Exception if unsupported string is requested.
      */
    def getTempEnum(name:String) : TEMP = {
      if(name.equalsIgnoreCase("HOT"))
        return HOT
      if(name.equalsIgnoreCase("COLD"))
        return COLD
      if(name.equalsIgnoreCase("FROZEN"))
        return FROZEN
      throw new RuntimeException("Unsupported food temperature type requested.")
    }
}