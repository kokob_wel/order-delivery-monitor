package com.orderdelivery.config

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by KOKOB WELDETENSAE on 3/11/20.
  * Configuration instance that captures config value specified in reference.conf or application.conf.
  */
object AppConfig {

  val config: Config = ConfigFactory.load()
  val root: Config = config.getConfig("orderdeliveryconfig")
  val orderConfig: Config = root.getConfig("order")
  val kitchenConfig: Config = root.getConfig("kitchen")
  val deliveryConfig: Config = root.getConfig("delivery")

  val inputFilePath = root.getString("input-file-path")

  val poissonDistributionRate = orderConfig.getDouble("poisson-distribution-rate")

  val coldShelfCapacity = kitchenConfig.getInt("cold-shelf-capacity")
  val hotShelfCapacity = kitchenConfig.getInt("hot-shelf-capacity")
  val frozenShelfCapacity = kitchenConfig.getInt("frozen-shelf-capacity")
  val overflowShelfCapacity = kitchenConfig.getInt("overflow-shelf-cpacity")
  val allowPickUpFromOverflow = kitchenConfig.getBoolean("allow-pickup-from-overflow-shelf")

  val minTrafficDelayInSeconds = deliveryConfig.getInt("min-traffic-delay-seconds")
  val maxTrafficDelayInSeconds = deliveryConfig.getInt("max-traffic-delay-seconds")

}