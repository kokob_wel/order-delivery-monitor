# Order Delivery Monitor

This application emulates the fulfillment of food orders in kitchen.
This system asynchronously reads food orders from a given json file.
In this system, orders are prepared by a kitchen and placed on a shelf
for drivers to pick up for delivery. The kitchen receives orders at a rate
following a configurable average value of Poisson Distribution per second
(lambda), makes the order (instant), then places the order on its correct shelf.
This application follows the Observer design pattern in such a way that
order producer is being observed by kitchen processing unit and delivery
processing unit.


## Build

Before you can build this project, you must install and configure the
following dependencies on your machine:

1. Java 1.8 or later
2. Scala 2.13.1 or later
3. sbt 1.3.8 or later

To BUILD this project, run this command from your project directory :

    sbt assembly

This will generate the .jar file into the `target/scala-2.13/` directory

## Test

To run the test locally use one of the following options

1. From terminal window, project directory, run "sbt test"
2. From terminal window, project directory, run "sbt assembly"
3. From your IDE, right click on EACH test class and select "Run Test".

## Run

To RUN this application, execute this command from `OrderDeliveryProject` directory :

    scala -cp target/scala-2.13/OrderDeliveryProject-assembly-0.0.1.jar com.orderdelivery.app.MainApp

##### Alternatively you can run the MainApp.Scala class from your IDE.


## Configuration Options

This application is configurable. You can set the following configuration
values to alter the behavior of this application.

1. poisson-distribution-rate
2. cold-shelf-capacity
3. hot-shelf-capacity
4. frozen-shelf-capacity
5. overflow-shelf-cpacity
6. allow-pickup-from-overflow-shelf

    
    orderdeliveryconfig{
        input-file-path = "src/main/resources/Orders.json"
        order{
            poisson-distribution-rate = 3.25
        }
        kitchen{
            cold-shelf-capacity = 15
            hot-shelf-capacity = 15
            frozen-shelf-capacity = 15
            overflow-shelf-cpacity = 20
            allow-pickup-from-overflow-shelf = true
        }
        delivery{
             min-traffic-delay-seconds = 2
            max-traffic-delay-seconds = 10
        }
    }

To pass your own application.conf file and override the project config file (shown above),
run this command from the `OrderDeliveryProjec/` directory :

    scala -Dconfig.file=file/path/to/application.conf -cp target/scala-2.13/OrderDeliveryProject-assembly-0.0.1.jar com.orderdelivery.app.MainApp

##### To override a specific config attribute of the project, use -D option. 
Example 1 - To alter poisson-distribution-rate, run this command from the `OrderDeliveryProjec/` directory :

    scala -Dorderdeliveryconfig.order.poisson-distribution-rate=2.75 -cp target/scala-2.13/OrderDeliveryProject-assembly-0.0.1.jar com.orderdelivery.app.MainApp

Example 2 - To alter shelf sizes, run this command from the `OrderDeliveryProjec/` directory :
 
     scala -Dorderdeliveryconfig.kitchen.cold-shelf-capacity=10 -Dorderdeliveryconfig.kitchen.hot-shelf-capacity=8 -cp target/scala-2.13/OrderDeliveryProject-assembly-0.0.1.jar com.orderdelivery.app.MainApp

 
## Description on how moving orders to and from the overflow shelf are handled

Moving of orders to and from the overflow shelf is done through a separate
function called `reshelf()`. In this application, there are three types
of Re-Shelving and they are all triggered whenever new order is placed and
order is removed from the shelfs.

1. Whenever the values of order items in any shelf degrades below zero.
2. Whenever any of the Cold, Hot or Frozen shelves reach their maximum storage capacity.
3. Whenever there are any order item in the Overflow shelf and there is
any available space in one of the Cold, Hot or Frozen shelves.

## Project Design

![](Project_Architecture.png)


## Documentation

To generate scala doc html files, run this command from `OrderDeliveryProjec/` directory : 

    sbt doc
    
Then open target/scala-2.13/api/index.html in a browser.
